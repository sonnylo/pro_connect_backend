const moment = require('moment');

function formatMessage(username, text, isBot = false) {
  return {
    username,
    text,
    isBot,
    time: moment().format('h:mm a')
  };
}

module.exports = formatMessage;