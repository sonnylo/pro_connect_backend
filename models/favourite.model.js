const sequelize = require("sequelize");
const db = require(".");

module.exports = (sequelize, DataTypes) => {
    const Favourite = sequelize.define('favourite',{
        user_id:{
            type: DataTypes.UUID
        },
        company_id: {
            type: DataTypes.UUID,
            references: {
                model: 'companies',
                key: 'id'
            }
        }
    },{
        modelName: 'favourites',
        freezeTablename: true,
        tableName: 'favourite_list',
        timestamps: false,
        underscored: true
    })

    Favourite.removeAttribute('id')
    // Favourite.associate = models => {
    //     Favourite.belongsTo(models.companies)
    // }


    Favourite.insert = (userid, companyid) => {
        const userID = userid
        const companyID = companyid
        return Favourite.findOrCreate({
            where:{
                user_id: userID,
                company_id: companyID
            },
            defaults: {
                user_id: userID,
                company_id: companyID
            }
        })
    }

    Favourite.associate = models => {
        Favourite.belongsTo(models.companies)
    }

    return Favourite
}