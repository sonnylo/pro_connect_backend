const db = require(".")

module.exports = (sequelize, DataTypes) => {
    const Companies = sequelize.define('companies', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true
        },
        company: {
            type: DataTypes.STRING
        },
        profile:{
            type: DataTypes.STRING
        },
        objective:{
            type: DataTypes.STRING
        },
        focus:{
            type: DataTypes.STRING
        },
        main_sector:{
            type: DataTypes.STRING
        },
        other_sector:{
            type: DataTypes.STRING
        },
        territories:{
            type: DataTypes.STRING
        },
        country:{
            type: DataTypes.STRING
        },
        website:{
            type: DataTypes.STRING
        },
        email:{
            type: DataTypes.STRING
        }
    },{
        modelName: 'companies',
        freezeTableName: true,
        tableName: 'companies',
        timestamps: false,
        underscored: true
    })

    Companies.associate = models => {
        Companies.hasMany(models.guest, {as: 'attendingGuests'})
        Companies.hasMany(models.badge, {as: 'filter_badge'})
    }
    
    return Companies
}