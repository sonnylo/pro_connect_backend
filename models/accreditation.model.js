module.exports = (sequelize, DataTypes) => {
    const Accreditation = sequelize.define('accreditation', {
        person_id: {
            type: DataTypes.UUID
        },
        name: {
            type: DataTypes.STRING
        },
        company_id:{
            type: DataTypes.UUID
        },
        arrival_on: {
            type: DataTypes.DATE
        },
        departure_on:{
            type: DataTypes.DATE
        }
    },{
        modelName: 'accreditation',
        freezeTableName: true,
        tableName: 'accreditation',
        timestamps: false,
    })

    return Accreditation
}