const guest = require('./guest.model.js')
const db = require('./index.js')

module.exports = (sequelize, DataTypes) => {
    const Badge = sequelize.define('badge', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
        },
        badge_title: {
            type: DataTypes.STRING
        }
    }, {
        modelName: 'badge',
        freezeTableName: true,
        tableName: 'badge',
        timestamps: false,
        underscored: true
    })

    Badge.associate = (models) => {
        Badge.hasMany(models.guest)
        Badge.belongsToMany(models.companies, {
            through: models.guest,
            as: 'filter_badge',
        })
    }

    return Badge
}
