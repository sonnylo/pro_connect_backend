module.exports = (sequelize, DataTypes) => {
    const Guest = sequelize.define('guest', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING
        },
        company_id: {
            type: DataTypes.UUID,
            references: {
                model: 'companies',
                key: 'id'
            }
        },
        badge_id: {
            type: DataTypes.UUID,
            references: {
                model: 'badge',
                key: 'id'
            }
        },
        arrival_on: {
            type: DataTypes.DATE
        },
        departure_on: {
            type: DataTypes.DATE
        }

    }, {
        modelName: 'guest',
        freezeTableName: true,
        tableName: 'guest',
        timestamps: false,
        underscored: true
    })

    Guest.associate = models => {
        Guest.belongsTo(models.badge)
        Guest.belongsTo(models.companies)
    }

 
    return Guest
}