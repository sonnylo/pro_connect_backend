// import the custom env for development mode
require('custom-env').env('production');

// load express framework and create an instance
const app = require('express')();

const cors = require('cors');

// load bodyparser helper
const bodyParser = require('body-parser');

// load http and create a server for the app
const http = require('http').Server(app)

// load socket.io and make it lsten to the http
const io = require('socket.io')(http);

// load node's build in file system to serve JSON
const fs = require('fs');

// get the port from the env file or default to 4000
const port = process.env.PORT || 4000;

// this is where all the routes are handled
const routes = require('./routes/routes.js')(app, fs);

const global = require('./chat_server/global')(io);

const db = require('./models')

const corsOptions = {
    exposedHeaders: 'auth-header',
};

app.use(cors(corsOptions));

// let clients = []

// configure express instance with body-parser and include handling json data
app.use(bodyParser.json);
app.use(bodyParser.urlencoded({ extended: true }));



http.listen(port, () => console.log(`Server running on ${process.env.APP_ENV} : ${port}`));