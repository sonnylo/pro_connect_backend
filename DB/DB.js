// import the custom env for development mode
require('custom-env').env('production');

// const Pool = require('pg').Pool;

const pg = require('pg');
// pg.defaults.ssl = true;
const Sequelize = require('sequelize')
const sequelize = new Sequelize(process.env.DB_TABLE, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'postgres',
    dialectOptions: {
      ssl: true,
      prependSearchPath: true
    }
})

// const sequelize = new Sequelize('test', 'sonny', 'admin', {
//   host: 'localhost',
//   port: 5432,
//   dialect: 'postgres',
//   protocol: 'postgres',
//   dialectOptions: {
//     prependSearchPath: true
//   }
// })

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize


