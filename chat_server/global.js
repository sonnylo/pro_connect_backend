// global socket server

const event_chat = require('./event_chat')

const globalChat = (io) => {

    const global = io;

    global.on('connection', socket => {
        const { id } = socket.client;
        // clients.push({"id": id, "name": })
        socket.on('joinRoom', ({name, event}) => {
            event_chat(global, socket, event, name, id)
        })


    })



}

module.exports = globalChat