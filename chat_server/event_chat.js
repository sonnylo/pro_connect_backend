const {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers,
    users
  } = require('../utils/users');

const formatMessage = require('../utils/message')

const botName = 'ProBot'

const eventChat = (global, socket, event, name, id) => {

    const user = userJoin(id, name, event)
    socket.join(event)
    socket.broadcast.emit('room message', formatMessage(botName, `${user.username} has entered the chat`, true))
    global.to(event).emit(event, getRoomUsers(event))
    // io.to(event).emit(event, console.log('test'))

    socket.on('room message', msg => {
        global.to(user.room).emit('room message', formatMessage(user.username, msg))
    })

    socket.on('leave room', ()=>{
        const user = userLeave(socket.id);

        if (user) {
            socket.leave(event, () => {
                global.to(user.room).emit('room message', formatMessage(botName, `${user.username} has left the chat`, true))
            })
        }
        global.to(event).emit(event, getRoomUsers(event))

    })

    socket.on('disconnect', () => {
        const user = userLeave(socket.id);
        if (user) {
            socket.leave(event, () => {
                global.to(user.room).emit('room message', formatMessage(botName, `${user.username} has left the chat`))
            })
        }
        global.to(event).emit(event, getRoomUsers(event))
    })

}

module.exports = eventChat;