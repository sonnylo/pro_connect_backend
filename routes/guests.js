const db = require('../models');
const { sequelize } = require('../models');


const guestsRoutes = (app, fs) => {

    // using local json data to mock API link for front end
    const dataPath = './data/data.json';

    app.get('/proconn/api/companies', async (req, res) => {
        // using node's file system to read the data as utf8 encoded.
        // callback takes an err and data param. If there is no err send data as json
        // fs.readFile(dataPath, 'utf8', (err,data) => {
        //     if(err){
        //         throw err;
        //     }
        //     res.setHeader('Access-Control-Allow-Origin', '*');
        //     res.send(JSON.parse(data));
        // })

        try{
            const guest = await db.companies.findAll({
                include: [
                    {
                        model: db.guest,
                        as: 'attendingGuests',
                        attributes:['id', 'name', 'arrival_on', 'departure_on'],
                        required: true,
                        include: [
                            {
                                model: db.badge,
                                attributes: [ 'badge_title'],
                                required: true
                            }   
                        ]
                    }
                ]
            }).then(companies => {
                let companylist = []
                companies.forEach(company => {
                    const filterSector = [
                        company.dataValues.main_sector,
                        ...company.dataValues.other_sector.split(',')
                    ]
                    // console.log(filterSector)
                    const data = {
                        "id": company.dataValues.id,
                        "company": company.dataValues.company,
                        "mainWorkingSector": company.dataValues.main_sector,
                        "otherWorkingSector": company.dataValues.other_sector,
                        "focus": company.dataValues.focus,
                        "territories": company.dataValues.territories,
                        "email": company.dataValues.email,
                        "website": company.dataValues.website,
                        "country": company.dataValues.country,
                        "companyProfile": company.dataValues.profile,
                        "objectives": company.dataValues.objective,
                        "attendingGuests": company.dataValues.attendingGuests,
                        "filterGuest":[...new Set(
                            company.dataValues.attendingGuests.map(guest => guest.name)
                        )],
                        "filterBadges": [...new Set(company.dataValues.attendingGuests.map(
                            guest => guest.badge.badge_title
                        ))],
                        "filterWorkingSector": [...new Set(
                            filterSector.map(sector => sector)
                        )]
                    }
                    companylist = [ ...companylist, data]
                })

                return companylist
            })
            // const guest = await db.guest.findAll({include: [{all: true}]})
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.send(guest)
        }catch(err){
            console.log(err)
        }

    });


};

module.exports = guestsRoutes;

