require('custom-env').env('production');
/*
    Here is where all the routes files will be held
*/

const guestsRoutes = require('./guests');
const favourites = require('./favourites')

const appRouter = (app, fs) => {

    // default (home) route
    app.get('/', (req, res) => {
        res.send('Welcome to the dev server of Pro Connect')
        // res.send(process.env.DB_HOST)
    });

    // guests list api routes, passing in the app and fs
    guestsRoutes(app, fs);
    
    favourites(app)
};

module.exports = appRouter;