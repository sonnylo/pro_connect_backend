const db = require('../models')

const favourites = (app) => {
    app.get('/proconn/api/favourites' , async(req, res) => {
        try{
            const list = await db.favourite.findAll({
                attributes:['user_id', 'company_id'],
                include:[{
                    model: db.companies,
                    required:true
                }
                ]
            })
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.send(list)
        }catch(err){
            console.log(err)
        }
    })

    app.post('/proconn/api/favourites', async(req, res) => {
        const user = req.query.user_id
        const company = req.query.company_id

        db.favourite.insert(user, company)
    })
}


module.exports = favourites